OBJECTS = Visualisation.cpp ConvexHull.cpp GiftOpening.cpp Culling.cpp getDataFromFile.cpp Point.cpp Edge.cpp Grid.cpp Performance.cpp

COMPILE = g++

INCLUDE_PATHS = -IC:\mingw_dev_lib\freeglut\include -"IC:\Users\IT-user\Documents\Concave Hull"
LIBRARY_PATHS = -LC:\mingw_dev_lib\freeglut\lib

COMPILER_FLAGS_BASE = -w -std=c++11 -pg -ggdb -O3
COMPILER_FLAGS_OMP = -fopenmp

LINKER_FLAGS = -lOpenGL32 -lglu32 -lfreeGLUT 
OUTPUT_NAME = ConcaveHullTest

all : $(OBJECTS)
	$(COMPILE) $(OBJECTS) $(INCLUDE_PATHS) $(LIBRARY_PATHS) $(COMPILER_FLAGS_BASE) $(LINKER_FLAGS) -o $(OUTPUT_NAME)

omp : $(OBJECTS)
	$(COMPILE) $(OBJECTS) $(INCLUDE_PATHS) $(LIBRARY_PATHS) $(COMPILER_FLAGS_BASE) $(COMPILER_FLAGS_OMP) $(LINKER_FLAGS) -o $(OUTPUT_NAME)
