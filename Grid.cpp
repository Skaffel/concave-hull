#include "Grid.h"

Grid::Grid(vector<Point> &vertices, double pointsPerBox, int numberOfThreads) {
	findExtremePoints(vertices); // Set minX, minY, maxX and maxY

	// Calculate the amount of boxes
	numberOfBoxesMaxAxis = sqrt(vertices.size() / pointsPerBox);

	worldWidth  = (maxX.x - minX.x);
	worldHeight = (maxY.y - minY.y);

	// use the largest dimension to calculate box size
	boxSize = (worldWidth > worldHeight)? worldWidth / numberOfBoxesMaxAxis : worldHeight / numberOfBoxesMaxAxis;

	numberOfBoxesX = (worldWidth > worldHeight)? numberOfBoxesMaxAxis : worldWidth  / boxSize;
	numberOfBoxesY = (worldWidth < worldHeight)? numberOfBoxesMaxAxis : worldHeight / boxSize;

	// Add an extra buffer of boxes to make sure all the extreme points get added to the grid and not outside
	numberOfBoxesMaxAxis += 1;
	numberOfBoxesX += 1;
	numberOfBoxesY += 1;

	vector<vector<vector<Point*>>> partialBoxes = vector<vector<vector<Point*>>>(numberOfThreads);	// Each element hold its own grid
	vector<thread> threads = vector<thread>(numberOfThreads);
	
	int size = vertices.size() / numberOfThreads;	// Determine size of each subset

	for (int n = 0; n < numberOfThreads; n++) {
		partialBoxes[n] = vector<vector<Point*>>(numberOfBoxesX * numberOfBoxesY);

		int start = n * size;
		int end = start + size;

		if (n == numberOfThreads - 1)
			end = vertices.size();

		// Partition this subset
		threads[n] = thread(&Grid::sortPartialSet, this, std::ref(vertices), std::ref(partialBoxes[n]), start, end);
	}

	for (int n = 0; n < numberOfThreads; n++) {
		threads[n].join();
	}

	// After all threads have completed, merge all the partial grids together
	boxes = vector<vector<Point*>>(numberOfBoxesX * numberOfBoxesY);
	for (int n = 0; n < boxes.size(); n++) {
		for (int i = 0; i < numberOfThreads; i++) {
			boxes[n].insert(boxes[n].end(), partialBoxes[i][n].begin(), partialBoxes[i][n].end());
		}
	}
}

void Grid::sortPartialSet(vector<Point> &vertices, vector<vector<Point*>> &boxes, int start, int end) {
	for (int n = start; n < end; n++) {
		int x = (vertices[n].x - minX.x) / boxSize;
		int y = (vertices[n].y - minY.y) / boxSize;
		int index = x + y * numberOfBoxesX;

		boxes[index].push_back(&vertices[n]);
	}
}

void Grid::findExtremePoints(vector<Point> &vertices) {
	minX = vertices[0];
	minY = vertices[0];
	maxX = vertices[0];
	maxY = vertices[0];

	for (int n = 0; n < vertices.size(); n++) {
		if (minX.x > vertices[n].x)
			minX = vertices[n];
		if (maxX.x < vertices[n].x)
			maxX = vertices[n];
		if (minY.y > vertices[n].y)
			minY = vertices[n];
		if (maxY.y < vertices[n].y)
			maxY = vertices[n];
	}
}

vector<Point> Grid::createBoxPoints() {
	vector<Point> boxPoints = vector<Point>();

	for (int x = 0; x < numberOfBoxesX; x++) {
		for (int y = 0; y < numberOfBoxesY; y++) {
			int index = x + y * numberOfBoxesX;

			if (boxes[index].size() > 0) {
				boxPoints.push_back(Point(x, y));
			}
		}
	}
	
	for (int n = 0; n < boxPoints.size(); n++) {
		boxPoints[n].clockwise = &boxPoints[n];
		boxPoints[n].counterClockwise = &boxPoints[n];
	}

	return boxPoints;
}