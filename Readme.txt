The soruce includes the code for generating a concave hull from a set of points as well as a simple freeGLUT visualisation file for testing the results. Visualisation.cpp is the "main function file" and will create a simple test case and visualise the concave boundary of that case. GiftOpening.h and .cpp is the main file, and entry point, used in the concave hull creation.

This code is not considered in any way to be of production value, therefore we will tell you about some caveats such things to hopefully easy some stress, maybe. 

	1. No exceptions are cought anywhere in the code

	2. ConvexHull.cpp uses quicksort to sort all the points. To use quicksort effectively we had to use a list of Point objects rather than points, otherwise quicksort would be very slow. Since we also have to create subsets of the vector, which if using big data sets or are unlucky, it will duplicate the amount of point objects as well. 

	3. -O3 compiler flag should be used, otherwise c++ std::vector will be extremely slow and inefficent

	4. Will create a grid containing all the edges created during the algorithm. This grid have unlimited extent and is currently not garbage collected. The grid will sadly contain both the final concave edges and edges created during the algorthm, which mean that prior to garabge collect this grid we would have to find which are in the final concave hull and not delete those. (edgeBoxes in GiftOpening.cpp)

	5. Currently, the concave part of the code will crash in case the size of the boxes are too small, or rather if the amount of boxes are too low. Why this is the case we do not know.

	6. If using multiple threads when calculating the concave hull: 
	The grid containing all the edges is shared between all threads if running the concave part on multiple threads. This may pose problems since the different threads will push to the same vector.

	The convex edges are boldly divided into different subsets. This will not guarantee a good parallelisation since one thread may get a set of edges which would take a long time to compute while another get edges which wont be needed to split at all. 

	The longest edges should be opened up in the beginning. If dividing up the edges, each thread will only locally sort on edge length which may reduce the quality of the concave hull, even if we have never seen this happening in out test cases.

	7. The quicksort algorithm is parallelized by using openMP, so it have to be included when compiling if you want to use the parallelization there. 