#include "getDataFromFile.h"

std::vector<Point> getPointsFromFile(std::string filename, int limit) {
    
    int count = 0;
    std::vector<Point> points;
    std::ifstream reader;
    std::string line;

    reader.open(filename);
    
    while (!reader.eof()) {
        std::getline(reader, line);
        
        std::istringstream sStream(line);
        std::string x, y, z;
        sStream >> x >> y >> z;

        if(isDouble(x) && isDouble(y)) {
            double xPos, yPos;
            std::istringstream(x) >> xPos; 
            std::istringstream(y) >> yPos; 
            points.push_back(Point(xPos, yPos));
        }
        count++;
        if(limit != 0 && count >= limit) {
            break;
        }
    }
    return points;
}


bool isDouble(std::string str) {
    double number;
    std::istringstream sStream(str);
    sStream >> std::noskipws >> number; 
    return !sStream.fail() && sStream.eof(); 
}

void writeRandomPointsToFile(std::string filename, int numberOfPoints) {
    double x, y, xTemp, yTemp;
    std::ofstream fs(filename); 
    srand (time(NULL));

    if(!fs)
    {
        std::cerr<<"Cannot open the output file."<<std::endl;
    }

    double randMax = RAND_MAX;
    for(int i=0; i<numberOfPoints; i++) {
        /*xTemp = (double)(rand()*1000 + 1000);
        yTemp = (double)(rand()*1000 + 1000);
        x = xTemp + yTemp;
        y = xTemp - yTemp;*/
        double t = 2*3.14159*rand()/randMax;
        double u = rand()/randMax + rand()/randMax;
        double r;
        if (u>1)
            r = 2-u;
        else
            r = u;
        x = 10000*cos(t);
        y = 10000*sin(t);

        fs << x << "\t" << y << "\t" << 0 << std::endl;
    }
    fs.close();
}

std::vector<Point> generateStar(int numberOfPoints) {
    std::vector<Point> points;
    srand (time(NULL));

    double randMax = RAND_MAX;

    int i=0;
    while (i<numberOfPoints){
        double x = 2*rand()/randMax-1;
        double y = 2*rand()/randMax-1;
        if (((x+1)*(x+1)+(y+1)*(y+1)>1)&&((x-1)*(x-1)+(y+1)*(y+1)>1)&&((x+1)*(x+1)+(y-1)*(y-1)>1)&&((x-1)*(x-1)+(y-1)*(y-1)>1)){       
            points.push_back(Point(x, y));
            i++;
        }
    }
    return points;
}

std::vector<Point> generateCrescent(int numberOfPoints) { //half-moon
    std::vector<Point> points;
    double x, y;
    srand (time(NULL));

    double randMax = RAND_MAX;

    int i=0;
    while (i<numberOfPoints){
        double t = 2*3.14159*rand()/randMax;
        double u = rand()/randMax + rand()/randMax;
        double r;
        if (u>1)
            r = 2-u;
        else
            r = u;
        x = 10000*cos(t)*r;
        y = 10000*sin(t)*r;

        if (((x-10000)*(x-10000)+(y)*(y)>200000000)){       
            points.push_back(Point(x, y));
            i++;
        }
    }
    return points;
}

std::vector<Point> generateSquare(int numberOfPoints) {
    std::vector<Point> points;
    double x, y;
    srand (time(NULL));

    double randMax = RAND_MAX;

    int i=0;
    for(int i=0; i<numberOfPoints; i++) {
        x = 1000*rand()/randMax;                
        y = 1000*rand()/randMax;
        points.push_back(Point(x, y));
    }
    return points;
}

std::vector<Point> generateCircle(int numberOfPoints) {
    std::vector<Point> points;
    double x, y, xTemp, yTemp;
    srand (time(NULL));

    double randMax = RAND_MAX;

    int i=0;

    for(int i=0; i<numberOfPoints; i++) {

        double t = 2*3.14159*rand()/randMax;        
        double u = rand()/randMax + rand()/randMax;
        double r;
        if (u>1)                                //put r=constant for all points on the boundary
            r = 2-u;
        else
            r = u;
        x = 10000*cos(t)*r;
        y = 10000*sin(t)*r;

        points.push_back(Point(x, y));
    }
    return points;
}

std::vector<Point> generateRandomPoints(int numberOfPoints) {
    std::vector<Point> points;
    double randMax = RAND_MAX;
    double constant=10;
    int i=0;
    double t=rand()/randMax+1;
    double u=rand()/randMax+1;
    while (i<numberOfPoints){
        double x=rand()/randMax;
        double y=rand()/randMax;
        if (sin(x*t*constant)*sin(y*u*constant)>0.4){
            points.push_back(Point(x, y));
            i++;
        }
    }
    return points;
}