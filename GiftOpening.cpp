#include "GiftOpening.h"

//
// Public
void GiftOpening::calculateConcaveHull(vector<Point> &vertices, vector<Point> &convexHull, vector<Edge*> &concaveHull) {
	// Constants
	int pointsPerBox = 40;					// Determines the size of each box in the grid (Avarage amount of points per box). We found 
											// values betweeen 40 - 80 works well in our test cases
	int numberOfThreadsGrid = 2;			// Number of threads used in the space partitioning
	int threadLevelsQuicksort = 2;			// Number of levels we should parallelize quicksort. Number of threads = levels ^ 2 
	double concaveThresholdModifier = 8;	// Determines the concaveness of the hull. Values between 5 - 10 seem to work well in our test cases
	double searchLimit = 0.2;				// Search limit of the concave phase. Percent of the maximum distance searched counted in boxes. 
											// A value of 0.2 seem to work fine
	int numberOfThreadsConcave = 2;			// Number of threads used by the concave part

	vector<Edge*> convexEdges;		// The convex hull
	vector<Point> boxPoints;		// Points given from the space partitioning. Used for culling
	Grid* grid;						// Equidistant grid for space paritioning of the points

	// Partition the points
    grid = new Grid(vertices, pointsPerBox, numberOfThreadsGrid);

    // Remove points which is in the interior of the convex hull to improve the performance when calculating the convex hull. 
    // The interesting points is saved in the convex hull vector
    convexHull = Culling::getCulledPoints(grid, boxPoints);

    // Use the divide and conquer method for calculating the convex hull. Use only the interesting points (convexHull) for imporved performance. 
    ConvexHull::createConvexHull(convexHull, threadLevelsQuicksort);

    // Change representation of the boundary from the linked list of points to a vector of edges
    convexEdges = getHullFromVertices(&convexHull[0], convexHull.size());

    // Caclulate the concave hull using the convex hull and the space partitioning.
   	concaveHull =  GiftOpening::convexToConcaveParallel(convexEdges, grid, concaveThresholdModifier, searchLimit, numberOfThreadsConcave);
}

vector<Edge*> GiftOpening::convexToConcaveParallel(vector<Edge*> &hullEdges, Grid* grid, double concaveThresholdModifier, double searchLimit, int numberOfThreads) {
	int size = hullEdges.size() / numberOfThreads;										// Determine the size of each subset of edges

	vector<vector<Edge*>> partialEdges = vector<vector<Edge*>>(numberOfThreads);		// Subset of convex edges
	vector<vector<Edge*>> finalPartialEdges = vector<vector<Edge*>>(numberOfThreads);	// Subset of the final concave edges
	vector<thread> threads = vector<thread>(numberOfThreads);

	// Grid which will contain all the edges. IS CURRENTLY NOT GARBAGE COLLECTED
	vector<vector<Edge*>> edgeBoxes = vector<vector<Edge*>>(grid->numberOfBoxesX * grid->numberOfBoxesY);

	// Divide the problem into different subsets.
	for (int n = 0; n < numberOfThreads; n++) {
		int start = n * size;
		int end = start + size;

		if (n == numberOfThreads - 1)
			end = hullEdges.size();

		partialEdges[n] = vector<Edge*>(hullEdges.begin() + start, hullEdges.begin() + end);

		// Start a new thread calculating hte concave hull from this convex subset
		threads[n] = thread(&GiftOpening::convexToConcave, std::ref(partialEdges[n]), std::ref(finalPartialEdges[n]), std::ref(edgeBoxes), std::ref(grid), concaveThresholdModifier, searchLimit);
	}

	for (int n = 0; n < numberOfThreads; n++) {
		threads[n].join();
	}

	// After all threads have completed, merge all the concave subsets together to a final solution.
	vector<Edge*> edges = vector<Edge*>();
	for (int i = 0; i < numberOfThreads; i++) {
		for (int n = 0; n < finalPartialEdges[i].size(); n++) {
			edges.insert(edges.end(), finalPartialEdges[i].begin(), finalPartialEdges[i].end());
		}
	}

	return edges;
}

void GiftOpening::convexToConcave(vector<Edge*> &hullEdges, vector<Edge*> &finalEdges, vector<vector<Edge*>> &edgeBoxes, Grid* grid, double concaveThresholdModifier, double searchLimit) {
	quicksort_edges(hullEdges, 0, hullEdges.size() - 1);	// Sort all edges depending on their length

	while (!hullEdges.empty()) {
		Edge* edge = hullEdges.back();
		hullEdges.pop_back();
		
		double dist = edge->length;

		// Determine the minimum distance for this edge. If the length of the edge is smaller it is considered to small to open up
		double minDist = calculateMaxEdgeDistanceLocal(grid, edge->a, concaveThresholdModifier);

		bool finalEdge = true;	// False if this edge is opened up and the hull is still a valid hull

		if (dist > minDist) {
			double bestAngle = 30;
			Point* pointA = edge->a;
			Point* pointB = edge->b;

			Point* bestPoint;
			findBestPoint(pointA, pointB, bestPoint, bestAngle, grid, searchLimit);	// Select new concave point on the boundary

			// Check and see if the angle is smaller than 2 (0.5 pi radians) and if the point is not already on the boundary
			if(bestAngle < 2 && !bestPoint->onBoundary) {
				Edge* edge1 = new Edge(pointA   , bestPoint);
				Edge* edge2 = new Edge(bestPoint, pointB);

				// Check and see if the new edges intersect with any other edge on the boundary. Sufficent check only one of the new edges
				if(!findIntersectionInEdgeBoxes(edge1, edgeBoxes, grid)) {
					addEdgeBoxes(edge1, edgeBoxes, grid);

					// Doing this again becouse i'm paranoid (Could possibly be removed)
					if(!findIntersectionInEdgeBoxes(edge1, edgeBoxes, grid)) {	
						// Seems like it is a valid move to open up this edge. Add the new edges to the list and add the new point to the boundary.
						sorted_insert(hullEdges, edge1);
						sorted_insert(hullEdges, edge2);
						
						bestPoint->onBoundary = true;

						finalEdge = false;
					}
				}
			}
		}

		// finalEdge will be false if it was not a valid move to oopen up the edge. In that case it belong to the final concave boundary.
		if (finalEdge)
			finalEdges.push_back(edge);
	}
}

// 
// Private

//
// Searching

void GiftOpening::findBestPoint(Point *pointA, Point *pointB, Point* &bestPoint, double &bestAngle, Grid* grid, double searchLimit){

	double x1 = pointA->x;
	double y1 = pointA->y;

	double x2 = pointB->x;
	double y2 = pointB->y;

	double dist = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)) * 2;

	int box1x = (x1 - grid->minX.x) / grid->boxSize;
	int box1y = (y1 - grid->minY.y) / grid->boxSize;

	int box2x = (x2 - grid->minX.x) / grid->boxSize;
	int box2y = (y2 - grid->minY.y) / grid->boxSize;

	double angle1to2 = ConvexHull::getAngle(pointA, pointB, 2); 	// Gives angles from 0 to 8
	
	// If both points are in the same box
	if(box1x == box2x && box1y == box2y){							
		findBestPointInBox(pointA, pointB, box1x, box1y, bestAngle, bestPoint, grid);

		if (angle1to2 > 4 && x1 + x2 > (grid->minX.x + (box1x + 1) * grid->boxSize - dist) * 2 && box1x < grid->numberOfBoxesX - 1) {	// Should search box to the right?
			findBestPointInBox(pointA, pointB, box1x + 1, box1y, bestAngle, bestPoint, grid);
		}

		if ((angle1to2 < 2 || angle1to2 > 6) && y1 + y2 < (grid->minY.y + box1y * grid->boxSize + dist) * 2 && box1y > 0) {		// Should search box below?
			findBestPointInBox(pointA, pointB, box1x, box1y - 1, bestAngle, bestPoint, grid);
		}
		
		if (angle1to2 < 4 && x1 + x2 < (grid->minX.x + box1x * grid->boxSize + dist) * 2 && box1x > 0) {								// Should search box to the left?
			findBestPointInBox(pointA, pointB, box1x-1, box1y, bestAngle, bestPoint, grid);
		}

		if (angle1to2 > 2 && angle1to2 < 6 && y1 + y2 > (grid->minY.y + (box1y + 1) * grid->boxSize - dist) * 2 && box1y < grid->numberOfBoxesY - 1) { // Should search box above?
			findBestPointInBox(pointA, pointB, box1x, box1y + 1, bestAngle, bestPoint, grid);
		}
	}

	// If both points are in boxes next to each other
	else if (box1x == box2x - 1 && box1y == box2y) {								// Box 2 to the right of box 1
		findBestPointInBox(pointA, pointB, box1x, box1y, bestAngle, bestPoint, grid);
		findBestPointInBox(pointA, pointB, box2x, box2y, bestAngle, bestPoint, grid);

		if (y1 + y2 < (grid->minY.y + box1y * grid->boxSize + dist) * 2 && box1y > 0){
			findBestPointInBox(pointA, pointB, box1x, box1y - 1, bestAngle, bestPoint, grid);
			findBestPointInBox(pointA, pointB, box2x, box2y - 1, bestAngle, bestPoint, grid);
		}
	}

	else if (box1x == box2x + 1 && box1y == box2y) {								// Box 2 to the left of box 1
		findBestPointInBox(pointA, pointB, box1x, box1y, bestAngle, bestPoint, grid);
		findBestPointInBox(pointA, pointB, box2x, box2y, bestAngle, bestPoint, grid);
		
		if (y1 + y2 > (grid->minY.y + (box1y + 1) * grid->boxSize - dist) * 2 && box1y < grid->numberOfBoxesY - 1) {
			findBestPointInBox(pointA, pointB, box1x, box1y + 1, bestAngle, bestPoint, grid);
			findBestPointInBox(pointA, pointB, box2x, box2y + 1, bestAngle, bestPoint, grid);
		}
	}

	else if (box1x == box2x && box1y == box2y - 1){									// Box 2 above box 1
		findBestPointInBox(pointA, pointB, box1x, box1y, bestAngle, bestPoint, grid);
		findBestPointInBox(pointA, pointB, box2x, box2y, bestAngle, bestPoint, grid);

		if (x1 + x2 > (grid->minX.x + (box1x + 1) * grid->boxSize - dist) * 2 && box1x < grid->numberOfBoxesX - 1) {
			findBestPointInBox(pointA, pointB, box1x + 1, box1y, bestAngle, bestPoint, grid);
			findBestPointInBox(pointA, pointB, box2x + 1, box2y, bestAngle, bestPoint, grid);
		}
	}

	else if (box1x == box2x && box1y == box2y + 1) {								// Box 2 below box 1
		findBestPointInBox(pointA, pointB, box1x, box1y, bestAngle, bestPoint, grid);
		findBestPointInBox(pointA, pointB, box2x, box2y, bestAngle, bestPoint, grid);

		if (x1 + x2 < (grid->minX.x+ box1x * grid->boxSize + dist) * 2 && box1x > 0){
			findBestPointInBox(pointA, pointB, box1x - 1, box1y, bestAngle, bestPoint, grid);
			findBestPointInBox(pointA, pointB, box2x - 1, box2y, bestAngle, bestPoint, grid);
		}
	}

	// Points are in different boxes, not next to each other
	else {
		int boxStartX;
		int boxStartY;
		int boxEndX;
		int boxEndY;

		if(box1x < box2x){
			boxStartX = box1x;
			boxEndX = box2x;
		}
		else{
			boxStartX = box2x;
			boxEndX = box1x;
		}
		if(box1y < box2y){
			boxStartY = box1y;
			boxEndY = box2y;
		}
		else{
			boxStartY = box2y;
			boxEndY = box1y;
		}

		// Loop through boxes between the two points
		for (int x = boxStartX; x <= boxEndX; x++){
			for (int y = boxStartY; y <= boxEndY; y++){
				findBestPointInBox(pointA, pointB, x, y, bestAngle, bestPoint, grid);
			}
		}

		// Calculate search length, the maximum amount of boxes we will extend the search before aborting
		int searchLength = 1 + grid->numberOfBoxesMaxAxis * searchLimit;

		// Extend search in case we did not find a good point in the first batch of boxes.
		for (int i = 0; bestAngle > 2 && i < searchLength; i++) {
			if(x1 < x2 && boxStartY > 0){
				boxStartY = boxStartY - 1;

				for (int n = boxStartX; n <= boxEndX; n++){
					findBestPointInBox(pointA, pointB, n, boxStartY, bestAngle, bestPoint, grid);
				}
			}
			if(x1 > x2 && boxEndY < grid->numberOfBoxesY - 1) {
				boxEndY = boxEndY + 1;

				for (int n = boxStartX; n <=boxEndX; n++){
					findBestPointInBox(pointA, pointB, n, boxEndY, bestAngle, bestPoint, grid);
				}
			}
			if(y1 < y2 && boxEndX < grid->numberOfBoxesX - 1) {
				boxEndX = boxEndX + 1;

				for (int n = boxStartY; n <= boxEndY; n++){
					findBestPointInBox(pointA, pointB, boxEndX, n, bestAngle, bestPoint, grid);
				}
			}
			if(y1 > y2 && boxStartX > 0) {
				boxStartX = boxStartX - 1;

				for (int n = boxStartY; n <= boxEndY; n++){
					findBestPointInBox(pointA, pointB, boxStartX, n, bestAngle, bestPoint, grid);
				}
			}
		}
	}
}

void GiftOpening::findBestPointInBox(Point *pointA, Point *pointB, int x, int y, double &bestAngle, Point* &bestPoint, Grid* grid) {
	// Its used to prevent problems that could happen if we would have a lot of points in a straight
	// line where a point would end up outside of the boundary (Probably some kind of truncation error). Could possibly be removed 
	// since we use doubles now instead of floats. 
	const double minorAngleContribution = 0.01;

	for (int n = 0; n < grid->boxes[x + y * grid->numberOfBoxesX].size(); n++) {		// Search the box the points are in.

		double alpha;
		double beta;
		anglesInTriangle(pointA, pointB, grid->boxes[x + y * grid->numberOfBoxesX][n], alpha, beta);
		
		if (alpha < beta) {
			alpha = beta  + minorAngleContribution * alpha;
		}
		else {
			alpha = alpha + minorAngleContribution * beta;
		}
		if (alpha < bestAngle) {
			bestAngle = alpha;
			bestPoint = grid->boxes[x + y * grid->numberOfBoxesX][n];
		}
	}
}

//
// Misc
void GiftOpening::addEdgeBoxes(Edge *edge, vector<vector<Edge*>> &edgeBoxes, Grid* grid) {
	double x1 = edge->a->x;
	double x2 = edge->b->x;
	double xStart, yStart, xEnd, yEnd, k, deltaY;

	// Calculate at what coordinates to start and end in
	if(x2 < x1) {
		xStart = x2;
		xEnd = x1;
		yStart = edge->b->y;
		yEnd = edge->a->y;
		k = (yEnd - yStart) / (xEnd - xStart);
	}
	else {
		xStart = x1;
		xEnd = x2;
		yStart = edge->a->y;
		yEnd = edge->b->y;
		k = (yEnd - yStart) / (xEnd - xStart);
	}

	// Calculate boxes corresponding to start and end coordinates
	int BoxX = (xStart - grid->minX.x) / grid->boxSize;
	int BoxXEnd = (xEnd - grid->minX.x) / grid->boxSize;
	int BoxY = (yStart - grid->minY.y) / grid->boxSize;
	int BoxYEnd = (yEnd - grid->minY.y) / grid->boxSize;

	double deltaX = grid->minX.x + (BoxX + 1) * grid->boxSize - xStart;

	if (k > 0) {
		deltaY = (grid->minY.y + (BoxY + 1) * grid->boxSize - yStart) / k;
	}
	else {
		deltaY = (grid->minY.y + BoxY * grid->boxSize - yStart) / k;
	}

	// Walk the boxes the line intersects with, until the end box is reached
	while (BoxX < grid->numberOfBoxesX && BoxY < grid->numberOfBoxesY && BoxY >= 0) {
		edgeBoxes[BoxX + BoxY * grid->numberOfBoxesX].push_back(edge);

		if (BoxX == BoxXEnd){
			if (BoxY == BoxYEnd) {		//Last box added
				return;
			}
			if (BoxY <= BoxYEnd) {
				BoxY += 1;
			}
			else {
				BoxY -= 1;
			}
		}
		else if (BoxY == BoxYEnd) {
			BoxX += 1;
		}
		else{
			if (deltaX < deltaY) {		//search to the right
				BoxX += 1;

				deltaY = deltaY - deltaX;
				deltaX = grid->boxSize;
			}
			else if (k < 0){			//search below
				BoxY -= 1;

				deltaX =  deltaX - deltaY;
				deltaY = -grid->boxSize * k;
			}
			else{						//search above
				BoxY += 1;

				deltaX = deltaX - deltaY;
				deltaY = grid->boxSize * k;
			}
		}
	}

	std::cout<<"Error: In addEdgeBoxes()"<<std::endl; //This should not happen. 
}

double GiftOpening::calculateMaxEdgeDistanceLocal(Grid* grid, Point* point, double concaveThresholdModifier) { 
	int look = 1;

	long int count = 0;
	int box1x = (point->x - grid->minX.x) / grid->boxSize;
	int box1y = (point->y - grid->minY.y) / grid->boxSize;

	// Loop through neighbouring boxes and find the box with highest density.
	for(int x = box1x - look; x < box1x + look + 1; x++) {
		for(int y = box1y - look; y < box1y + look + 1; y++) {

			if(0 <= x && 0 <= y && x < grid->numberOfBoxesX && y < grid->numberOfBoxesY) {
				int currentBoxSize = grid->boxes[x + y * grid->numberOfBoxesX].size();
				
				if(count < currentBoxSize) {
					count = currentBoxSize;
				} 
			}
		}
	}

	return grid->boxSize / sqrt(count) * concaveThresholdModifier;
}

vector<Edge*> GiftOpening::getHullFromVertices(Point* origin, int max) {
	vector<Edge*> edges;

	int number = 0;
    Point* node = origin;
    do {
        edges.push_back(new Edge(node, node->clockwise));

        number++;
        node = node->clockwise;
    } while (node != origin && number < max);

    return edges;
}

//
// Sorting

void GiftOpening::sorted_insert(vector<Edge*> &boundary, Edge *edge) {
  for(int i = boundary.size() - 1; i > 0; i--) {
    if(edge->length > boundary[i]->length) {
      if(edge != boundary[i + 1])
        boundary.insert(boundary.begin() + i + 1, edge);  

      return;
    }
  }

  boundary.insert(boundary.begin(), edge);
}

void GiftOpening::quicksort_edges(std::vector<Edge*> &array, int start, int stop) {
  if(start < stop) {
    int pivotIndex = start;
    int pivotNewIndex = partition_edge(array, start, stop, pivotIndex);

    quicksort_edges(array, start, pivotNewIndex - 1);
  	quicksort_edges(array, pivotNewIndex + 1,  stop);
  }
}

int GiftOpening::partition_edge(std::vector<Edge*> &array, int start, int stop, int pivotIndex) {
  Edge* tempValue;
  Edge* pivotValue = array[pivotIndex];
  array[pivotIndex] = array[stop];
  array[stop] = pivotValue;

  int storeIndex = start;
  for(int i = start; i < stop; i++) {
    if(array[i]->length < pivotValue->length) {

      tempValue = array[i];                    
      array[i]  = array[storeIndex];
      array[storeIndex] = tempValue;
      storeIndex++;
    }
  }

  array[stop] = array[storeIndex];
  array[storeIndex] = pivotValue;

  return storeIndex;
}

//
// Geometry

void GiftOpening::anglesInTriangle(Point *pointA, Point *pointB, Point *pointC, double &alpha, double &beta) {
	if ((pointA->x == pointC->x && pointA->y == pointC->y) || (pointB->x == pointC->x && pointB->y == pointC->y)) {
		alpha = 10;
		beta = 10;
	}
	else{
		double a3 = ConvexHull::getAngle(pointC, pointB, 0);
		double a2 = ConvexHull::getAngle(pointA, pointB, a3);
		double a1 = ConvexHull::getAngle(pointA, pointC, a2);
		
		alpha = a1 - a2;
		beta  = a2 - a3;
	}
}

//
// Line intersection
bool GiftOpening::findIntersectionInEdgeBoxes(Edge *edge, vector<vector<Edge*>> &edgeBoxes, Grid* grid) {
	double x1 = edge->a->x;
	double x2 = edge->b->x;
	double xStart, yStart, xEnd, yEnd, k, deltaY;

	// Coordinates to start and end at. 
	if(x2 < x1) {
		xStart = x2;
		xEnd = x1;
		yStart = edge->b->y;
		yEnd = edge->a->y;
		k = (yEnd - yStart) / (xEnd - xStart);		// Find slope
	}
	else {
		xStart = x1;
		xEnd = x2;
		yStart = edge->a->y;
		yEnd = edge->b->y;
		k = (yEnd - yStart) / (xEnd - xStart);		// Find slope
	}

	// Corresponding boxes to start and end at. 
	int BoxX = (xStart - grid->minX.x) / grid->boxSize;
	int BoxXEnd = (xEnd - grid->minX.x) / grid->boxSize;
	int BoxY = (yStart - grid->minY.y) / grid->boxSize;
	int BoxYEnd = (yEnd - grid->minY.y) / grid->boxSize;

	double deltaX = grid->minX.x + (BoxX + 1) * grid->boxSize - xStart;

	if (k > 0) {
		deltaY = (grid->minY.y + (BoxY + 1) * grid->boxSize - yStart) / k;
	}
	else {
		deltaY = (grid->minY.y + BoxY * grid->boxSize - yStart) / k;
	}

	// Search for intersections
	while (BoxX < grid->numberOfBoxesX && BoxY < grid->numberOfBoxesY && BoxY >= 0) {
		if (lineIntersectionBox(edge, edgeBoxes[BoxX + BoxY * grid->numberOfBoxesX])) {
			std::cout<<"intersection detected"<<std::endl;
			return true;
		}
		if (BoxX == BoxXEnd){
			if (BoxY == BoxYEnd){		//Last box searched
				return false;
			}
			if (BoxY <= BoxYEnd){
				BoxY += 1;
			}
			else {
				BoxY -= 1;
			}
		}
		else if (BoxY == BoxYEnd){
			BoxX += 1;
		}
		else{
			if (deltaX < deltaY){		//search to the right
				BoxX += 1;

				deltaY = deltaY - deltaX;
				deltaX = grid->boxSize;
			}
			else if (k < 0) {			//search below
				BoxY -= 1;

				deltaX =  deltaX - deltaY;
				deltaY = -grid->boxSize * k;
			}
			else{					//search above
				BoxY += 1;

				deltaX = deltaX - deltaY;
				deltaY = grid->boxSize * k;
			}
		}
	}
	std::cout<<"Error: In findIntersectionInEdgeBoxes()"<<std::endl; //This should not happen
	
	return true;
}

bool GiftOpening::lineIntersectionBox(Edge *edge, vector<Edge*> &edgeBox) {
	for (int i = 0; i < edgeBox.size(); i++)
		if (lineIntersection(edge, edgeBox[i]))
			return true;
	return false;
}

bool GiftOpening::lineIntersection(Edge *edge1, Edge *edge2){
	double p0_x=edge1->a->x;
	double p0_y=edge1->a->y;
	double p1_x=edge1->b->x;
	double p1_y=edge1->b->y;
	double p2_x=edge2->a->x;
	double p2_y=edge2->a->y;
	double p3_x=edge2->b->x;
	double p3_y=edge2->b->y;
    double s1_x, s1_y, s2_x, s2_y;

    s1_x = p1_x - p0_x;
    s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;
    s2_y = p3_y - p2_y;

    double s, t;
    double div=-s2_x * s1_y + s1_x * s2_y;

    if (div==0) {
    	return false; //edges parallel
    }

    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / div;
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / div;

    if (s > 0 && s < 1 && t > 0 && t < 1){
        return true;	// Collision detected
    }

    return false; // No collision
}