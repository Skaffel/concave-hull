#ifndef PERFORMANCE_H
#define PERFORMANCE_H 

#include <map>
#include <vector>
#include <string>
#include <chrono>
#include <windows.h>

using std::map;
using std::vector;
using std::string;

class Performance {
private:
	static double get_cpu_time();
	static double get_wall_time();

public:
	static map<string, vector<double>> times;

	static void startMeasure(string key);
	static void stopMeasure(string key);

	static void saveMeasurement(string key, double value);

	static vector<double> getPerformance(string key);
	static double getMean(string key);
	static double getVariance(string key);

};


#endif