#ifndef __Edge_H_INCLUDED__ 
#define __Edge_H_INCLUDED__

#include <math.h>

#include "Point.h"

/**
*	Class representing an edge using two points.
*/
class Edge {
public:
    Point *a;
    Point *b;

    double length;

    Edge();
    Edge(Point *pA, Point *pB);

    bool containPoint(Point* p);
};

#endif