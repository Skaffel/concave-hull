#ifndef GIFT_OPENING_H
#define GIFT_OPENING_H

#include <iostream>
#include <vector>
#include <thread>
#include <math.h>

#include "Point.h"
#include "Edge.h"

#include "Grid.h"

#include "ConvexHull.h"
#include "Culling.h"

using std::vector;

/**
*	Class with functions to calculate the concave hull 
*/
class GiftOpening {
private: 
	//
	// Searching

	/**	Searches all relevant boxes for the best point. The best point is the one that has the smallest 
	* 	max angle of alfa and beta (given by the anglesInTriangle function) from the two given points. 
	*
	*	@param pointA Point A
	*	@param pointB Point B
	*	@param bestAngle return parameter of the angle of the best point found
	*	@param betPoint return parameter of the best point found
	*	@param grid contains the space partitioned points
	*	@param searchlimit Maximum percentage of the boxes we will search through to find good potential point
	*/
	void static findBestPoint(Point *pointA, Point *pointB, Point* &bestPoint, double &bestAngle, Grid* grid, double searchlimit);

	/**	Search a box for the best point in the box. The best point is the one that has the smallest 
	* 	max angle of alfa and beta (given by the anglesInTriangle function) from the two given points. 
	*
	*	@param pointA A Point
	*	@param pointB B Point
	*	@param x x coordinate (=index) of the box to search
	*	@param y y coordinate (=index) of the box to search
	*	@param bestAngle return parameter of the angle of the best point found
	*	@param betPoint return parameter of the best point found
	*	@param grid contains the space partitioned points
	*/
	void static findBestPointInBox(Point *pointA, Point *pointB, int x, int y, double &bestAngle, Point* &bestPoint, Grid* grid);

	//
	// Misc

	/**	Adds an Edge to all the Edgeboxes it intersects. 
	*
	*	@param edge the Edge to add to Edgeboxes
	*	@param edgeBoxes the edge will be added to all boxes it intersects
	*	@param grid contains the space partitioned points
	*/
	void static addEdgeBoxes(Edge *edge, vector<vector<Edge*>> &edgeBoxes, Grid* grid);

	/**	Calculates the local maximum edge distance based on the density of points near the given point. Calculates the
	*	max distance by looking at the boxes around the point. In the densest box it calculates the average closest 
	*	distance between points and multiplies that with a constant (concaveThresholdModifier) to get the max edge length. 
	*
	*	@param grid is the grid the points are partitioned in
	*	@param point the local point to base the calculation from
	*	@param concaveThresholdModifier modifies the minimum concave edge length
	*/
	double static calculateMaxEdgeDistanceLocal(Grid* grid, Point* point, double concaveThresholdModifier);

	/** Creates a vector with edges, representing a hull, from point which belongs to a linked list on the boundary. Used to 
	*	change the representation between the convex and concave phase of the algorithm.
	*
	*	@param &origin	Initial on the boundary
	*	@param &max Max number of points on the boundary, if it reaches above this it will abort because something went wrong.
	*	@return a vector containing edges of the hull 
	*/
	vector<Edge*> static getHullFromVertices(Point* origin, int max);

	//
	// Sortning

	/** Inserts a Edge* into a std::vector<Edge*> so that the vector remains sorted after Edge->length
	*	Asumes that the vector is already sorted. 
	*
	* 	@param &boundary the sorted vecotor to insert the Edge* into
	*	@param edge the Edge* to insert
	*/
	void static sorted_insert(vector<Edge*> &boundary, Edge *edge);

	/** Sorts a std::vector containing Edge* after increasing length, found by Edge->length
	*	Uses the Quicksort algorithms and uses function partition_edge to split on pivot element
	*
	*	@param &array the vector of Edge* to be sorted. 
	*	@param start the index to start on.
	*	@param stop the index to stop on.
	*/
	void static quicksort_edges(vector<Edge*> &array, int start, int stop);

	/** Helper function to quicksort_edges. Puts all the elements smaller than pivot value to 
	*	the left in the vector and all the elements bigger to the right in the vector. 
	*
	*	Edge->length is used to compare elements. 
	*	@param &array the vector of Edge* to be sorted. 
	*	@param start the index to start on.
	*	@param stop the index to stop on.
	*	@param pivotIndex the index of the pivot element
	*	@return the new pivot index
	*/
	int static partition_edge(vector<Edge*> &array, int start, int stop, int pivotIndex);

	//
	// Geometry

	/**	Finds the angles between AC AB and BC BA, where A, B and C are Points. The angles calculated are
	*	the inner angles in the triangle when A, B and C are clockwise ordered. 
	*
	*	@param pointA Point A
	*	@param pointB Point B 
	*	@param pointC Point C
	*	@param alpha return parameter for angle AC AB
	*	@param beta return parameter for angle BC BA 
	*/
	void static anglesInTriangle(Point *pointA, Point *pointB, Point *pointC, double &alpha, double &beta);

	//
	// Line intersection

	/**	Searches Edgeboxes for an intersection with the given edge.
	*
	*	@param edge the Edge to find an intersection for
	*	@param edgeBoxes the Edgeboxes (vector<Edge*>)
	*	@param grid contains the space partitioned points
	*/
	bool static findIntersectionInEdgeBoxes(Edge *edge, vector<vector<Edge*>> &edgeBoxes, Grid* grid);

	/** Finds out if an Edge intersects any Edge in a given box of Edges.
	*
	*	@param edge The Edge
	*	@param edgeBox The box of Edges
	*	@return true if Edge intersects any Edge in Edgebox
	*/
	bool static lineIntersectionBox(Edge *edge, vector<Edge*> &edgeBox);

	/** Finds out if two edges (lines) instersect eachother. 
	*
	*	@param edge1 The first Edge
	*	@param edge2 The second Edge
	*	@return true if the edges intersect each other
	*/
	bool static lineIntersection(Edge *edge1, Edge *edge2);

public:
	/**	Calculates the concave hull given a vector of points, given as a vector of edges. It will first partition the points
	*	into an equidistant grids, remove points whihc cannot be on the convex boundary, calcualte the convex hull before opening
	*	up the convex hull to create the concave hull. 
	*	
	*	Note: Not all of the points in the returned convex vector will be on the convex boundary. The first and last element of the vector
	*	are always on the boundary. The next point, either clockwise or counterclockwise, on the boundary for any given boundary point
	*	can be retrieved from any point on the convex boundary.
	*
	*	Note: The function is full of parameters, feel free to change and try different values : )
	*
	*	Warning: Will create a grid containing all the edges created during the algorithm. This grid have unlimited extent
	*	and is currently not garbage collected. The grid will sadly contain both the final concave edges and edges created during the algorthm, 
	*	which mean that prior to garabge collect this grid we would have to find which are in the final concave hull and not delete those.
	*
	*	Warning: If using multiple threads when calculating the concave hull: 
	*	The grid containing all the edges is shared between all threads if running the concave part on multiple threads. This may pose
	*	problems since the different threads will push to the same vector.
	*
	*	The convex edges are boldly divided into different subsets. This will not guarantee a good parallelisation since one thread may
	*	get a set of edges which would take a long time to compute while another get edges which wont be needed to split at all. 
	*
	*	The longest edges should be opened up in the beginning. If dividing up the edges, each thread will only locally sort on edge length 
	*	which may reduce the quality of the concave hull.
	*
	*	@param vertices (Input) contains the points which the concave hull should be calcualted for
	*	@param &convexHull (Output)	contains point which may potentially be on the convex boundary.
	*								First and last point will alwyas be on the boundary.
	*	@param &concaveHull (Output) contains all the edges on the concave hull
	*/
	void static calculateConcaveHull(vector<Point> &vertices, vector<Point> &convexHull, vector<Edge*> &concaveHull);

	/** 
	*	Divides a list of convex edges into several subsets and open them all up seperatly on a new thread. Each thread will then return a subset
	*	of the concave hull. The concave hull from the threads finally be merged together. 
	*
	*	Warning: Will create a grid containing all the edges created during the algorithm. This grid have unlimited extent
	*	and is not garbage collected. The grid will sadly contain both the final concave edges and edges created during the algorthm, 
	*	which mean that prior to garabge collect this grid we would have to find which are in the final concave hull and not delete those.
	*
	*	Warning: The grid containing all the edges is shared between all threads if running the concave part on multiple threads. This may pose
	*	problems since different threads will push to the same vector.
	*
	*	Warning: The convex edges are boldly divided into different subsets. This will not guarantee a good parallelisation since one thread may
	*	get a set of edges which would take a long time to compute while another get edges which wont be needed to split at all. 
	*	
	*	@param &hullEdges (Input) the convex hull 
	*	@param grid (Input) contains the space partitioned points
	*	@param concaveThresholdModifier modifies the minimum concave edge length
	*	@param searchlimit Maximum percentage of the boxes we will search through to find good potential point
	*	@param numberOfThreads Number of threads used to calculate the concave hull
	*/
	vector<Edge*> static convexToConcaveParallel(vector<Edge*> &hullEdges, Grid* grid, double concaveThresholdModifier, double searchLimit, int numberOfThreads);

	/** Takes a convex hull as a vector of edges and calculates the concave hull by opening up convex edges. The minimum length 
	*	for opening up a concave part are determined by local avarage distance between points multiplied with a constant. 
	*	To improve performance the points used to calculate the concave hull have to be space partitioned. The algorithm will 
	*	create a new grid containing the edges. This grid is used to check new edges against to prevent hull edge intersections. 
	*
	*	Warning: Will create a grid containing all the edges created during the algorithm. This grid have unlimited extent
	*	and is not garbage collected. The grid will sadly contain both the final concave edges and edges created during the algorthm, 
	*	which mean that prior to garabge collect this grid we would have to find which are in the final concave hull and not delete those.
	*
	*	@param &hullEdges (Input) the convex hull 
	*	@param &finalEdges (Output) The returned concave hull
	*	@param &edgeBoxes  (Output) contanins boxes with edges. Used to prevent line intersections. 
	*	@param grid (Input) contains the space partitioned points
	*	@param concaveThresholdModifier modifies the minimum concave edge length
	*	@param searchlimit Maximum percentage of the boxes we will search through to find good potential point
	*/
	void static convexToConcave(vector<Edge*> &hullEdges, vector<Edge*> &finalEdges, vector<vector<Edge*>> &edgeBoxes, Grid* grid, double concaveThresholdModifier, double searchLimit);
};

#endif