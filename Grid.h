#ifndef GRID_H
#define GRID_H

#include <vector>
#include <thread>
#include <iostream>

#include "Point.h"

#include "Performance.h"

using std::vector;
using std::thread;

/**
*	Class for partition points into an equidistant grid
*/
class Grid {
private:
	/**
	*	Set the points with the maximum and minimum x and y values, minX, minY, maxX and maxY.
	*
	*	@param &vector Vector to find extreme points from
	*/
	void findExtremePoints(vector<Point> &vertices);

	/**
	*	Partitions all points, given a world and box size.
	*	
	*	@param &vertices Points to be partitioned
	*	@param &boxes The grid of points
	*	@param start index of the point vector
	*	@param end index of the point vector
	*/
	void sortPartialSet(vector<Point> &vertices, vector<vector<Point*>> &boxes, int start, int end);
public:
	double worldWidth;		// Width of the world: maxX - minX
	double worldHeight;		// Height of the world maxY - minY

	double boxSize;	// The size of each box, determined by world size and pointsPerBox

	int numberOfBoxesMaxAxis;	// Number of boxes on the longest axis
	int numberOfBoxesX;			// Number of boxes on x-axis
	int numberOfBoxesY;			// Number of boxes on y-axis
	
	Point minX;	// Point with minimum x value
	Point minY; // Point with minimum y value
	Point maxX; // Point with maximum x value
	Point maxY;	// Point with maximum y value

	/**
	* Grid of boxes containing the space partitioned points
	*/
	vector<vector<Point*>> boxes;

	/**
	*	Creates a grid using the given points. The grid will contain the partitioned points.
	*	
	*	@param &vertices is a vector containing hte points to be partitioned
	*	@param pointsPerBox is the avarage number of points per box, will be used to calculate the amount of boxes in the grid
	*	@param numberOfThreads is the number of threads that will be used to partition the points
	*/
	Grid(vector<Point> &vertices, double pointsPerBox, int numberOfThreads);

	/**
	*	Creates a vector of points based on the coordinates of the boxes in the grid which contain at least one point.
	*
	*	@return	list of points representing the coordinates for each box in the grid
	*/
	vector<Point> createBoxPoints();
};

#endif