#ifndef VISUALISATION_H
#define VISUALISATION_H

#include "getDataFromFile.h"

#include "GiftOpening.h"

#include "Point.h"
#include "Edge.h"

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <chrono>

#include "Performance.h"
#include <string>

using std::string;

int SCREEN_WIDTH = 1680;
int SCREEN_HEIGHT = 1050;

float scale = 1;
Point displacement = Point();

void render();

void setCamera(vector<Point> &vertices);

void mouseEvent(int button, int state, int x, int y);
void mouseMotionEvent(int x, int y);
void mousePassiveEvent(int x, int y);

void renderBoundary(Point* origin, int max, float r, float g, float b);
void renderEdge(Point* A, Point* B, float r, float g, float b);
void renderEdge(Edge* edge, float r, float g, float b);
void renderPoint(Point* point, float r, float g, float b);

vector<Point> vertices;
vector<Point> convexHull;
vector<Edge*> concaveHull;

/**
*   Run to calculate and visualizise the concave hull algorithm. Press left mousebutton and drag mouse to pan and use scrollwheel to zoom.
*
*   You may change the vertices data in this function to test different set of points, either by creating new sets or by reading from a file.
*/
int main(int argc, char* args[]) {
    // Initialize GLUT
    glutInit(&argc, args);
    glutInitContextVersion(2, 1);

    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    glutCreateWindow("Convex Hull");

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glClearColor(1.f, 1.f, 1.f, 1.f);

    // 
    // Create / read test data and create the concave hull
    cout<<"Read data"<<endl;
    vertices = generateCrescent(100000);                                    // Generate or read data

    cout<<"Calculate concave hull"<<endl;
    GiftOpening::calculateConcaveHull(vertices, convexHull, concaveHull);   // Calculate concave hull

    setCamera(vertices);                                                    // Set scale and translation of the camera

    // Initialize event functions and run GLUT
    glutDisplayFunc(render);
    glutMouseFunc(mouseEvent);
    glutMotionFunc(mouseMotionEvent);
    glutPassiveMotionFunc(mousePassiveEvent);

    glutMainLoop();

    return 0;
}

void setCamera(vector<Point> &vertices) {
    for (int n = 0; n < vertices.size(); n++) {
        displacement = displacement + vertices[n];
    }

    displacement = displacement / vertices.size();

    for (int n = 0; n < vertices.size(); n++) {
        if (abs(vertices[n].x - displacement.x) > scale)
            scale = abs(vertices[n].x - displacement.x);
        if (abs(vertices[n].y - displacement.y) > scale)
            scale = abs(vertices[n].y - displacement.y);
    }

    scale *= 2;
}

Point mouse = Point();
Point origin = Point();

void render() {
    glClear(GL_COLOR_BUFFER_BIT);

    glColor3f(1, 1, 1); 
    glPointSize(1.0);
    glLineWidth(3.0);

    for (int n = 0; n < vertices.size(); n++) {
       renderPoint(&vertices[n], 0.f, 0.f, 0.f);
    }

    renderBoundary(&convexHull[0], convexHull.size(), 0.0f, 1.0f, 0.0f);
    
    for (int n = 0; n < concaveHull.size(); n++) {
        renderEdge(concaveHull[n]->a, concaveHull[n]->b, 1.f, 0.f, 0.f);
    }

    glutSwapBuffers();
}

void renderBoundary(Point* origin, int max, float r, float g, float b) {
    
    int number = 0;
    Point* node = origin;
    do {
        number++;

        renderEdge(node, node->clockwise, r, g, b);
        renderPoint(node, r, g, b);

        node = node->clockwise;

    } while(node != origin && number < max);
}

void renderEdge(Point* A, Point* B, float r, float g, float b) {
    glBegin(GL_LINES);
        glColor3f(r,g,b);
        glVertex2f((A->x - displacement.x) / scale, (A->y - displacement.y) / scale);
        glVertex2f((B->x - displacement.x) / scale, (B->y - displacement.y) / scale);
    glEnd();
}

void renderEdge(Edge* edge, float r, float g, float b) {
    glBegin(GL_LINES);
        glColor3f(r,g,b);
        glVertex2f((edge->a->x - displacement.x) / scale, (edge->a->y - displacement.y) / scale);
        glVertex2f((edge->b->x - displacement.x) / scale, (edge->b->y - displacement.y) / scale);
    glEnd();
}

void renderPoint(Point* point, float r, float g, float b) {
    glBegin(GL_POINTS);
        glColor3f(r,g,b);
        glVertex2f((point->x - displacement.x) / scale, (point->y - displacement.y) / scale);
    glEnd();
}

bool button0 = false;

void mouseMotionEvent(int x, int y) {
    mouse.x = displacement.x + (2.0 * x / SCREEN_WIDTH  - 1) * scale;
    mouse.y = displacement.y - (2.0 * y / SCREEN_HEIGHT - 1) * scale;

    if (button0) {
        displacement.x = displacement.x - (mouse.x - origin.x);
        displacement.y = displacement.y - (mouse.y - origin.y);
    }

    mouse.x = displacement.x + (2.0 * x / SCREEN_WIDTH  - 1) * scale;
    mouse.y = displacement.y - (2.0 * y / SCREEN_HEIGHT - 1) * scale;

    origin = mouse;

    render();
}

void mousePassiveEvent(int x, int y) {
    mouse.x = displacement.x + (2.0 * x / SCREEN_WIDTH  - 1) * scale;
    mouse.y = displacement.y - (2.0 * y / SCREEN_HEIGHT - 1) * scale;

    origin = mouse;

    render();
}

void mouseEvent(int button, int state, int x, int y) {
    mouse.x = displacement.x + (2.0 * x / SCREEN_WIDTH  - 1) * scale;
    mouse.y = displacement.y - (2.0 * y / SCREEN_HEIGHT - 1) * scale;

    if (button == 3 || button == 4) {
        int direction = (button == 3)? -1 : 1;

        scale += scale * direction / 10;

        if (scale < 0)
            scale = 0;
    }

    if (button == 0) {
        button0 = (state == 0)? true : false;

       if (state == 0) {
            origin = mouse;
        }
    }

    mouse.x = displacement.x + (2.0 * x / SCREEN_WIDTH  - 1) * scale;
    mouse.y = displacement.y - (2.0 * y / SCREEN_HEIGHT - 1) * scale;

    render();
}

#endif
