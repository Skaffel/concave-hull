#include "ConvexHull.h"

void ConvexHull::createConvexHull(vector<Point> &convexVertices, int threadLevels) {
	// Sort all points using quicksort
	#pragma omp parallel
	{
		#pragma omp single
		{
			quicksort_points_x_parallel(convexVertices, 0, convexVertices.size() - 1, threadLevels);
		}
	}

	// Reinitialize: Make sure all points point toward themselves
	for (int n = 0; n < convexVertices.size(); n++) {
		convexVertices[n].clockwise = &convexVertices[n];
		convexVertices[n].counterClockwise = &convexVertices[n];
	}

	// Merge the points together, setting their clockwise and counterclockwise neighbours
	mergePoints(convexVertices, 0, convexVertices.size());
}

void ConvexHull::mergePoints(vector<Point> &vertices, int start, int end) {
	int localSize = end - start;

	// Dont divide and merge if the size is smaller than 1
	if (localSize > 1) {
		int midPoint = start + localSize / 2;

		// Divide phase: Divide the points into two subsets
		mergePoints(vertices, start, midPoint);
		mergePoints(vertices, midPoint, end);

		//
		// Merge phase: Merge the two subsets together

		// Points are sorted so we know these points will be neighblouring points
		Point* leftBoundary  = &vertices[midPoint - 1];
		Point* rightBoundary = &vertices[midPoint];

		// Start and end of the upper common tangent of the two convex hulls, initially set to be any valid edge between the two hulls that dont cross any line in either hull
		Point* upperStart = leftBoundary;
		Point* upperEnd   = rightBoundary;

		// Start and end of the lower common tangent of the two convex hulls, initially set to be any valid edge between the two hulls that dont cross any line in either hull
		Point* lowerStart = rightBoundary;
		Point* lowerEnd   = leftBoundary;

		// Find the common tangents
		traverseLeft (upperStart, upperEnd,  1, false, upperStart, upperEnd);		// Find upper common tangent
		traverseRight(lowerEnd, lowerStart, -1, false, lowerEnd, lowerStart);		// Find lower common tangent

		// Set the points in the common tangents to point toward the other hull, thus merging the two convex hulls togheter.  
		upperStart->clockwise = upperEnd;
		upperEnd->counterClockwise = upperStart;

		lowerStart->clockwise = lowerEnd;
		lowerEnd->counterClockwise = lowerStart;
	}
}

void ConvexHull::traverseLeft(Point* &left, Point* &right, int direction, bool lastFail, Point* startLeft, Point* startRight) {
	Point* nextVertex = (direction > 0)? left->counterClockwise : left->clockwise;		// Step clokwise or counterclockwise depending on direction

	double winding = (*nextVertex - *right)^(*left - *right);							// Find the winding of the step using a "2D" cross product
	bool validStep = winding * direction >= 0 && nextVertex != startLeft;				// Valid step if the winding is in a valid direction and the next point is not the starting point
	
	if (validStep)
		left = nextVertex;			// Complete the step if it was valid

	// Take a step on the right side, either if this step is valid or if the last step was valid. Two failed steps in a row will exit the algorithm since we found the common tangent.
	if (validStep || !lastFail)
		traverseRight(left, right, direction, !validStep, startLeft, startRight);
}

void ConvexHull::traverseRight(Point* &left, Point* &right, int direction, bool lastFail, Point* startLeft, Point* startRight) {
	Point* nextVertex = (direction < 0)? right->counterClockwise : right->clockwise;	// Step clokwise or counterclockwise depending on direction

	double winding = (*nextVertex - *left)^(*right - *left);							// Find the winding of the step using a "2D" cross product
	bool validStep = winding * direction <= 0 && nextVertex != startRight;				// Valid step if the winding is in a valid direction and the next point is not the starting point

	if (validStep)
		right = nextVertex;			// Complete the step if it was valid

	// Take a step on the right side, either if this step is valid or if the last step was valid. Two failed steps in a row will exit the algorithm since we found the common tangent.
	if (validStep || !lastFail)
		traverseLeft(left, right, direction, !validStep, startLeft, startRight);
}

void ConvexHull::quicksort_points_x_parallel(vector<Point> &array, int start, int stop, int levels) {
	if(start < stop) {
		int middle = start + (stop-start)/2;
		int pivotIndex = start;

		int pivotNewIndex = partition_points_x(array, start, stop, pivotIndex);
		
		if(levels > 0) {
			#pragma omp task shared(array) 
			{
				quicksort_points_x_parallel(array, start, pivotNewIndex - 1, levels - 1);
			}
			#pragma omp task shared(array)
			{
				quicksort_points_x_parallel(array, pivotNewIndex + 1, stop, levels - 1);

			}
		}
		else {
			quicksort_points_x(array, start, pivotNewIndex - 1);
			quicksort_points_x(array, pivotNewIndex + 1, stop);
		}
		
	}
}

void ConvexHull::quicksort_points_x(vector<Point> &array, int start, int stop) {
	if(start < stop) {
		int middle = start + (stop-start)/2;
		int pivotIndex = start;

		int pivotNewIndex = partition_points_x(array, start, stop, pivotIndex);

		quicksort_points_x(array, start, pivotNewIndex - 1);
		quicksort_points_x(array, pivotNewIndex + 1, stop);
	}
}

int ConvexHull::partition_points_x(vector<Point> &array, int start, int stop, int pivotIndex) {
	Point tempValue;
	Point pivotValue = array[pivotIndex];

	array[pivotIndex] = array[stop];
	array[stop] = pivotValue;

	int storeIndex = start;
	for(int i = start; i < stop; i++) {
		if(array[i].x < pivotValue.x || (array[i].x == pivotValue.x && array[i].y < pivotValue.y)) {
			tempValue = array[i];                    

			array[i] = array[storeIndex];
			array[storeIndex] = tempValue;

			storeIndex++;
		}
	}

	array[stop] = array[storeIndex];
	array[storeIndex] = pivotValue;

	return storeIndex;
}

void ConvexHull::giftWrapping(Point *start, Point *end, double startAngle, vector<Point> &interestingPoints, vector<Point*> &hullPoints) {
	int i = 0;
	
	Point* pointOnHull = start;
	Point* nextPoint;

	double bestAngle;
	double oldAngle = startAngle - 2;
	double testAngle;

	while (true) {
		hullPoints.push_back(pointOnHull);

		pointOnHull->onBoundary = true;
		
		nextPoint = &interestingPoints[0];
		bestAngle = getAngle(pointOnHull, nextPoint, oldAngle);

		for (int j = 1; j < interestingPoints.size(); j++) {

			testAngle = getAngle(pointOnHull, &interestingPoints[j], oldAngle);

			if (bestAngle > testAngle) {
				bestAngle = testAngle;

				nextPoint = &interestingPoints[j];
			}
			else if (testAngle == bestAngle) {
				double dist1 = (pointOnHull->x - nextPoint->x) * (pointOnHull->x - nextPoint->x) + (pointOnHull->y - nextPoint->y) * (pointOnHull->y - nextPoint->y);
				double dist2 = (pointOnHull->x - interestingPoints[j].x) * (pointOnHull->x - interestingPoints[j].x) + (pointOnHull->y - interestingPoints[j].y) * (pointOnHull->y - interestingPoints[j].y);
				
				if (dist2 < dist1) {
					bestAngle = testAngle;
					nextPoint = &interestingPoints[j];
				}
			}
		}

		i++;

		oldAngle = bestAngle - 2;
		pointOnHull = nextPoint;

		if (nextPoint == end) {
			hullPoints.push_back(pointOnHull);
			pointOnHull->onBoundary = true;
			
			return;
		}
	}
}

double ConvexHull::getAngle(Point *pointA, Point *pointB, double oldAngle) {
	double angle;
	double x=pointB->x-pointA->x;
	double y=pointB->y-pointA->y;
	double xaddy=x+y;
	double xsuby=x-y;
	if (x==0 && y==0){			//if point A and B have same coordinates
		angle=oldAngle+40;		//This will return a angle value that is higher than if the points don't have the same coordinates
		return angle;
	}
	else if (0<xaddy){
		if (0<xsuby){
			angle=-y/x;			//angle between -1 and 1
		}
		else{
			angle=x/y+6.0;		//angle between 5 and 7
		}
	}
	else {
		if (0<xsuby){
			angle=x/y+2.0;		//angle between 1 and 3
		}
		else{
			angle=-y/x+4.0;		//angle between 3 and 5
		}
	}
	while (angle>=oldAngle+8){		//adjust angle counterclockwise if more than one revolution higher than oldAngle
		angle=angle-8.0;
	}
	while (angle<oldAngle){		//adjust angle clockwise if lower than oldAngle
		angle=angle+8.0;
	}
	return angle;
}