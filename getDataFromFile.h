#ifndef GET_DATA_FROM_FILE_H
#define GET_DATA_FROM_FILE_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <time.h>
#include <stdlib.h>

#include "Point.h"

std::vector<Point> getPointsFromFile(std::string filename, int limit);
bool isDouble(std::string str);
void writeRandomPointsToFile(std::string filename, int numberOfPoints);

std::vector<Point> generateRandomPoints(int numberOfPoints);
std::vector<Point> generateCircle(int numberOfPoints);
std::vector<Point> generateSquare(int numberOfPoints);
std::vector<Point> generateCrescent(int numberOfPoints);
std::vector<Point> generateStar(int numberOfPoints);


#endif
