#ifndef __Point_H_INCLUDED__ 
#define __Point_H_INCLUDED__ 

#include <math.h>

class Point {
public:
    double x;
    double y;

    bool onBoundary;				// If this point is on the boundary or not

    Point();
    Point(Point *point);
    Point(double x, double y);
        
    Point* counterClockwise;		// The next point on the convex boundary, counterclockwise of this point 
    Point* clockwise;				// The next point on the convex boundary, clockwise of this point

    /**
    *	Return true if the two points have the same coordinates
    */
    bool equals(Point* other);

    /**
	* Return true if the point have the same coordinates as either its neighbours
	*/
    bool neighbourConflict();
        
    inline double squaredLength() const {
   		return x * x + y * y;
	}

    inline double length() const {
    	return sqrt(squaredLength());
	}

	inline Point left() const {
		return Point(-y, x);
	}

	inline Point right() const {
		return Point(y, -x);
	}

	inline bool operator==(const Point &other) const {
		return x == other.x && y == other.y;
	}

	inline Point operator+(const Point &other) const {
		return Point(x + other.x, y + other.y);
	}

	inline Point operator-(const Point &other) const {
		return Point(x - other.x, y - other.y);
	}

	inline Point operator*(const double &scale) const {
		return Point(scale * x, scale * y);
	}

	inline Point operator/(const double &scale) const {
		return Point(x / scale, y / scale);
	}

	inline double operator*(const Point& other) const {
		return x * other.x + y * other.y;
	} 

	inline double operator^(const Point& other) const {
		return x * other.y - y * other.x;
	}

    double distance(Point *p);
};

#endif