#include "Point.h"

Point::Point() {
        this->x = 0;
        this->y = 0;

        this->clockwise = this;
        this->counterClockwise = this;

        this->onBoundary = false;
}

Point::Point(Point *point) {
        this->x = point->x;
        this->y = point->y;

        this->clockwise = this;
        this->counterClockwise = this;

        this->onBoundary = point->onBoundary;
}

Point::Point(double x, double y) {
        this->x = x;
        this->y = y;

        this->clockwise = this;
        this->counterClockwise = this;

        this->onBoundary = false;
}

double Point::distance(Point *p) {
        double diffX = this->x - p->x;
        double diffY = this->y - p->y;
        return sqrt(diffX*diffX + diffY*diffY);
}

bool Point::equals(Point* other) {
        return this->x == other->x && this->y == other->y;
}

bool Point::neighbourConflict() {
        return equals(counterClockwise) || equals(clockwise) || counterClockwise->equals(clockwise);
}