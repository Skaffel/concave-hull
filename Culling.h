#ifndef CULLING_H
#define CULLING_H

#import <vector>

#import "Point.h"
#import "Grid.h"

#import "ConvexHull.h"

using std::vector;

class Culling {
private:
	/**
	* Return true if the given point is within a given polygon. The polygon is given as a convex hull, repsesented by a circular linked list.
	*
	*	@param origin is one point of the polygon
	*	@param &P is the point that will be checked if it is in the boundary or not
	*/
	static bool pointInPolygon(Point* origin, const Point &P);

	/**
	*	Removes all points that are "outside" of a given edge / line. 
	*
	*	@param startPoint is the starting point of the line
	*	@param endPoint is the ending point of the line
	*	@param &culledBoxes (Input) is the boxes that are to be checked if they should be culled (by the line)
	*	@param &interestingPoints (Output) is the vector of points that were not culled
	*	@param grid is the grid object containing all information about the space partitioning
	*/
	static void lineCulling(Point &startPoint, Point &endPoint, vector<Point> &culledBoxes, vector<Point> &interestingPoints, Grid* grid);

	/**
	*	Return true if this box is "outiside" a line or not, ie if the points in this box are interesting points from a convex hull perspective or not.
	*
	*	@param start is the start point of the linge
	*	@param end is the end point of the line
	*	@param box is the vertice of the current box that is to be checked against the line
	*	@param grid is the grid object containing all information about the space partitioning
	*	@return true ig this box is outside the line or not.
	*/
	static bool boxOnLine(Point &start, Point &end, Point &box, Grid* grid);

public:
	/**
	*	Will remove points that are not potential points to be on the convex boundary, ie "interior" points. This function is used to cull points before calculating
	*	the convex boundary to remove the complexity of that problem. It will create a new list of verices by finding which points and boxes from the space partitioning
	*	are not in the "interior" of the hull. It will not find all interior points, since then the convex hull already be found, but it will usually be able to remove many points.
	*
	*	@param grid is the grid object containing all information about the space partitioning
	*	@param &boxPoints (Output: Debugging) is a vector which contain the convex boundary of boxes in the grid, which cointains at least one point. Used for debugging.
	*	@return a vector of interesting points that can be used to calculate the convex hull, points from the original set that are in the "interior" of the hull have been culled.  
	*/
	static vector<Point> getCulledPoints(Grid* grid, vector<Point> &boxPoints);
};

#endif