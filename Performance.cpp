#include "Performance.h"

map<string, vector<double>> Performance::times;

void Performance::startMeasure(string key) {
	times[key].push_back(get_wall_time()); 
}


void Performance::stopMeasure(string key) {
	int index = times[key].size() - 1;

	times[key][index] = get_wall_time() - times[key][index];
}

void Performance::saveMeasurement(string key, double value) {
	times[key].push_back(value);
}

vector<double> Performance::getPerformance(string key) {
	return times[key];
}

double Performance::getMean(string key) {
	vector<double> measurements = times[key];

	double mean = 0;

	for (int n = 0; n < measurements.size(); n++) {
		mean += measurements[n];
	} 

	return mean / measurements.size();
}

double Performance::getVariance(string key) {
	double mean = getMean(key);
	vector<double> measurements = times[key];

	double variance = 0;

	for (int n = 0; n < measurements.size(); n++) {
		variance += (measurements[n] - mean) * (measurements[n] - mean);
	}

	return variance / measurements.size();
}

double Performance::get_cpu_time() {
    FILETIME a,b,c,d;
    if (GetProcessTimes(GetCurrentProcess(),&a,&b,&c,&d) != 0){
        //  Returns total user time.
        //  Can be tweaked to include kernel times as well.
        return
            (double)(d.dwLowDateTime |
            ((unsigned long long)d.dwHighDateTime << 32)) * 0.0000001;
    }else{
        //  Handle error
        return 0;
    }
}

double Performance::get_wall_time() {
    LARGE_INTEGER time,freq;
    if (!QueryPerformanceFrequency(&freq)){
        //  Handle error
        return 0;
    }
    if (!QueryPerformanceCounter(&time)){
        //  Handle error
        return 0;
    }
    return (double)time.QuadPart / freq.QuadPart;
}
