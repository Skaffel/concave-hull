#include "Culling.h"

vector<Point> Culling::getCulledPoints(Grid* grid, vector<Point> &boxPoints) {
	boxPoints = grid->createBoxPoints();						// Create a list of vertices using the boxes in the grid, where the coordinates of the points are the coordinate of each box.

	ConvexHull::mergePoints(boxPoints, 0, boxPoints.size());	// Merge the box points, creating a convex hull. No need to sort them first since they are already sorted. 

	bool failedBuffer = false;	// True if the first culling method fail

	vector<Point> cullingBoundary;
	vector<Point> buffedCullingBoundary;

	int number = 0;				// Just used to prevent any infinite loops, in case a boundary would go kapusch
	Point* node = 0;
	Point* origin = 0;

	// Loop through convex boundary of the box points and create a vector from the linked list
	number = 0;
	node = &boxPoints[0];
	do {
		Point point = Point(node->x, node->y);

		point.counterClockwise = node->counterClockwise;
		point.clockwise = node->clockwise;

		cullingBoundary.push_back(point);
		buffedCullingBoundary.push_back(point);

		number++;
		node = node->clockwise;

	} while(node != &boxPoints[0] && number < boxPoints.size());

	// Use the convex hull of boxes to create a smaller hull, a buffered boundary. This is because we will remove all boxes within this boundary and to 
	// prevent any accidents where we would remove a box which acually have points on the convex boundary we will created this buffer boundary to prevent this from happening. 
	// It is created by simply moving each hullsegment one step into the interior of the hull. 
	for (int n = 0; n < cullingBoundary.size(); n++) {
		Point A = cullingBoundary[n];
		Point B = cullingBoundary[(n + 1) % cullingBoundary.size()];

		buffedCullingBoundary[n].x += (A.y >= B.y)? 0 : 1;
		buffedCullingBoundary[n].y += (A.x <= B.x)? 0 : 1;

		buffedCullingBoundary[n].x += (A.y <= B.y)? 0 : -1;
		buffedCullingBoundary[n].y += (A.x >= B.x)? 0 : -1;

		buffedCullingBoundary[(n + 1) % buffedCullingBoundary.size()].x += (A.y >= B.y)? 0 : 1;
		buffedCullingBoundary[(n + 1) % buffedCullingBoundary.size()].y += (A.x <= B.x)? 0 : 1;

		buffedCullingBoundary[(n + 1) % buffedCullingBoundary.size()].x += (A.y <= B.y)? 0 : -1;
		buffedCullingBoundary[(n + 1) % buffedCullingBoundary.size()].y += (A.x >= B.x)? 0 : -1;
	}

	// Loop through the original convex hull of boxes, and upadte the coordinates so this boundary become the buffered boundary
	number = 0;
	node = &boxPoints[0];
	do {
		node->x = buffedCullingBoundary[number].x;
		node->y = buffedCullingBoundary[number].y;

		number++;
		node = node->clockwise;

	} while(node != &boxPoints[0] && number < boxPoints.size());

	// During the creation of the buffed boundary there is a risk that it is no longer convex, or have other problems. Therefore we loop through the boundary and remove 
	// any points on the boundary that make it fail. 
	number = 0;
	node = &boxPoints[0];
	origin = &boxPoints[0];
	bool removedBad = false;
	do {
		removedBad = false;
		double winding = (*node->counterClockwise - *node)^(*node->clockwise - *node);		// Check winding of this segment

		if (winding < 0 || node->neighbourConflict()) {										// Remove this point if the winding is bad or if the point have a neighbour conflict
			node->counterClockwise->clockwise = node->clockwise;
			node->clockwise->counterClockwise = node->counterClockwise;

			origin = node->clockwise;														// Reset the loop to make sure the entire boundary is correct

			removedBad = true;

			number = 0;
		}

		if (node->counterClockwise == node || node->clockwise == node) {					// The boundary is too small, it has failed!
			failedBuffer = true;
		}

		number++;
		node = node->clockwise;

	} while((node != origin || removedBad) && number < boxPoints.size() && !failedBuffer);

	// list of boxes which hold only the potential convec hull pointss
	vector<Point> culledBoxes = vector<Point>();

	// Only add the boxes that are not in the interior of the buffered convex hull. In case the buffered hull failed, add all boxes.
	if (!failedBuffer) {
		for (int x = 0; x < grid->numberOfBoxesX; x++) {
			for (int y = 0; y < grid->numberOfBoxesY; y++) {
				Point P = Point(x, y);

				if (!pointInPolygon(&boxPoints[0], P)) {
					culledBoxes.push_back(P);

					
				}
			}
		}
	}
	// Add all boxes since the buffered convex hull failed
	else {
		for (int x = 0; x < grid->numberOfBoxesX; x++) {
			for (int y = 0; y < grid->numberOfBoxesY; y++) {
				culledBoxes.push_back(Point(x, y));
			}
		}
	}

	// List containing the final points used to calculate the convex hull
	vector<Point> interestingPoints = vector<Point>();

	// Use line culling using a quadrilateral spanned by the minimum and maximum points of the dataset to remove points interior of thes quadrilateral. We dont need to create a buffer 
	// for this since we know those 4 points are on the boundary. Use line culling four times for each side in the quadrilateral to add only the points that are interesting.
	lineCulling(grid->minX, grid->maxY, culledBoxes, interestingPoints, grid);
	lineCulling(grid->maxY, grid->maxX, culledBoxes, interestingPoints, grid);
	lineCulling(grid->maxX, grid->minY, culledBoxes, interestingPoints, grid);
	lineCulling(grid->minY, grid->minX, culledBoxes, interestingPoints, grid);

	return interestingPoints;
}

void Culling::lineCulling(Point &start, Point &end, vector<Point> &culledBoxes, vector<Point> &interestingPoints, Grid* grid) { 
	double startAngle = ConvexHull::getAngle(&start, &end, 0);

	for (int n = 0; n < culledBoxes.size(); n++) {
		int index = culledBoxes[n].x + culledBoxes[n].y * grid->numberOfBoxesX;

		if (boxOnLine(start, end, culledBoxes[n], grid)) {			// Only check the points in the box if it is not interior
			for (int i = 0; i < grid->boxes[index].size(); i++) {
				double angle = ConvexHull::getAngle(&end, grid->boxes[index][i], startAngle);

				if (angle - startAngle >= 4){
					interestingPoints.push_back(Point(grid->boxes[index][i]));
				}
			}
		}
	}
}

bool Culling::boxOnLine(Point &start, Point &end, Point &box, Grid* grid) {
	double leftX  = (start.x < end.x)? start.x : end.x;
	double rightX = (start.x > end.x)? start.x : end.x;

	double lowerY = (start.y < end.y)? start.y : end.y;
	double upperY = (start.y > end.y)? start.y : end.y;

	double worldX = box.x * grid->numberOfBoxesX + grid->minX.x;
	double worldY = box.y * grid->numberOfBoxesX + grid->minY.y;

	if (worldX < leftX && rightX < worldX)
		return false;
	if (worldY < lowerY && upperY < worldY)
		return false;
	return true;
}

bool Culling::pointInPolygon(Point* origin, const Point &P) {
	Point paddedNode = Point();
	Point paddedNext = Point();

	Point* node = origin;		// Loop through all the points in teh polygon
	do {
		paddedNode.x = node->x;
		paddedNode.y = node->y;

		paddedNext.x = node->clockwise->x;
		paddedNext.y = node->clockwise->y;

		Point normal = (paddedNext - paddedNode).left();	// Calculate the normal

		if ((normal.x != 0 || normal.y != 0) && normal * (P - paddedNode) >= 0)		// If the node is on the wrong side of this segemnt of the polygon, it is not in the polygon
			return false;

		node = node->clockwise;
	} while (node != origin);

	return true;	// No seperating axis was found, the point is inside the polygon
}