#include "Edge.h"

Edge::Edge() {
	this->a = new Point();
	this->b = new Point();
}

Edge::Edge(Point *pA, Point *pB) {
	this->a = pA;
	this->b = pB;

	double diffX = (this->a->x - this->b->x);
	double diffY = (this->a->y - this->b->y);
	
	length = sqrt(diffX*diffX + diffY*diffY);
}

bool Edge::containPoint(Point* p) {
	return a == p || b == p;
}
