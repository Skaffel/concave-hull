#ifndef CONVEX_HULL_H
#define CONVEX_HULL_H

#include <iostream>
#include <cmath>
#include <math.h>
#include <vector>
#include <chrono>
#include <Windows.h> //Only needed for timing functions
#include <thread>

#include "Point.h"

#include "Performance.h"

using std::cout;
using std::endl;
using std::vector;

/**
*	Class with functions to calculate the convex hull. Includes both a gifwrapping algorithm and an algorithm based on divide and conquer.
*/
class ConvexHull {
private:
	/**
	*	Function that will can be used to find the upper and lower common tangents. It will traverse one step either clockwise or counterclockwise on the left
	*	hull that is being merged, depending on the given direction. Function will recursively call traverse right. It will have found the common tangent when it cannot take
	*	another step in either direction, either because the next step would create anedge that intersects the hull or because the next point is the starting point (would happen if all points are collinear)
	*
	*	@param &left is the current point on the left hull. Will be either the start or end of the common tangent depedning if we find the upper or lower.
	*	@param &right is the current point on the right hull. Will be either the start or end of the common tangent depedning if we find the upper or lower.
	*	@param direction is the direction, determines if we want to find the upper or lower tangent. (Pos -> upper, Neg -> lower)
	*	@param lastFail	is true if the last traversal failed, if it fails twice in a row mean it cannot talk a step in either direction and the upper and lower common tangent is found
	*	@param startLeft is the starting point on the left hull, used to break if we have traversed the entire hull in case all points would be collinear
	*	@param startRight is the starting point on the right hull, used to break if we have traversed the entire hull in case all points would be collinear
	*/
	static void traverseLeft (Point* &left, Point* &right, int direction, bool lastFail, Point* startLeft, Point* startRight);

	/**
	*	Function that will can be used to find the upper and lower common tangents. It will traverse one step either clockwise or counterclockwise on the right
	*	hull that is being merged, depending on the given direction. Function will recursively call traverse left. It will have found the common tangent when it cannot take 
	*	another step in either direction, either because the next step would create an edge that intersects the hull or because the next point is the starting point (would happen if all points are collinear)
	*
	*	@param &left is the current point on the left hull. Will be either the start or end of the common tangent depedning if we find the upper or lower.
	*	@param &right is the current point on the right hull. Will be either the start or end of the common tangent depedning if we find the upper or lower.
	*	@param direction is the direction, determines if we want to find the upper or lower tangent. (Pos -> upper, Neg -> lower)
	*	@param lastFail	is true if the last traversal failed, if it fails twice in a row mean it cannot talk a step in either direction and the upper and lower common tangent is found
	*	@param startLeft is the starting point on the left hull, used to break if we have traversed the entire hull in case all points would be collinear
	*	@param startRight is the starting point on the right hull, used to break if we have traversed the entire hull in case all points would be collinear
	*/
	static void traverseRight(Point* &left, Point* &right, int direction, bool lastFail, Point* startLeft, Point* startRight);

	/** Sorts a std::vector containing points after ther x coordinate. Bigger x values will be placed in the end of the vector, in case of same x value, y will be used instead.
	*	Uses the Quicksort algorithm and uses function partition_points to split on pivot element. Will start new threads on each level until the limit is reached.
	*
	*	@param &array the vector of points to be sorted. 
	*	@param start the index to start on.
	*	@param stop the index to stop on.
	*	@levels is the depth of parallelisation, number of threads is levels^2
	*/
	static void quicksort_points_x_parallel(vector<Point> &array, int start, int stop, int levels);

	/** Sorts a std::vector containing points after ther x coordinate. Bigger x values will be placed in the end of the vector, in case of same x value, y will be used instead.
	*	Uses the Quicksort algorithm and uses function partition_points to split on pivot element. Not parallelised. 
	*
	*	@param &array the vector of points to be sorted. 
	*	@param start the index to start on.
	*	@param stop the index to stop on.
	*/
	static void quicksort_points_x(vector<Point> &array, int start, int stop);
	
	/** Helper function to quicksort_points_x and quicksort_points_x_parallel. Puts all the elements smaller than pivot value to 
	*	the left in the vector and all the elements bigger to the right in the vector. 
	*
	*	Elements are sorted according to lexiographic x order.	
	* 
	*	@param &array the vector of points to be sorted. 
	*	@param start the index to start on.
	*	@param stop the index to stop on.
	*	@param pivotIndex the index of the pivot element
	*	@return the new pivot index
	*/
	static int partition_points_x(vector<Point> &array, int start, int stop, int pivotIndex);
public:
	/**
	*	Creates the convex hull using a divide and conquer method. It consists of a quicksort phase and a merge phase. Set of points can contain duplicate points
	*	
	*	@param &convexVertices (Input) is the set of points which should be used to calculate the convex boundary
	*	@param threadLevels is the depth the quicksort algorithm should be parallelized. Number of threads will be levels^2
	*/
	static void createConvexHull(vector<Point> &convexVertices, int threadLevels);

	/**
	*	Merges a sorted set of points together to form convex hulls, which in turn will be merged together. The points have to be sorted so that in the sorted axis, 
	*	two neighbouring points in the vector have to be neighbouring points in the set as well. Will recursively call itself to merge subsets of the set into convex hulls
	*	until the complete convex hull is reaced. 
	*
	*	The output will be stored in the points since all points have a reference to their counterclockwise and clockwise neighbour on the hull. For each point, these references have
	*	to initially be set to reference themselves. 
	*
	*	@param	&vertices contains the vertices to be merged together into one convec hull. Boundary will be stored as clockwise and counterclockwise neighbours in each point. 
	*					  Traversing this linked list will give the complete convex boundary.
	*	@param	start is the starting index of the vertices vector that should be used to merge
	*	@param	end is the end index that should be used to be merged together
	*/
	static void mergePoints(vector<Point> &vertices, int start, int end);

	/**
	*	Uses the gift wrapping algorithm to calculate the convex boundary between the start and stop points, which does not have to create a full continous hull which makes
	*	it find a "convex" hull. Both the start and stop points have to be on the boundary and the given start angle have to be given for the start point. An initial startAngle have to be given
	*	which will act as the previous "edge". When giving this angle it only need to be on the correct side, ie half of all angles would work. 
	*
	*	In case we would start the algorithm with the leftmost point we should start with an angle of 6 (Use "linearized" angle from 0 - 8), while if starting with the point with maximum y
	*	coordinate we should start with an angle of 0.
	*
	*	@param start is the start point of the boundary
	*	@param end is the end point of this boundary segment, if same as start it will find the entire convex boundarys
	*	@param startAngle is the starting angle for teh start point, given in a linearized angle. 
	*	@param &interestingPoints (Input) is the set of points we want to use to calculate the boundary
	*	@param &hullPoints (Output) is a list of points which are on teh convex hull
	*/
	static void giftWrapping(Point *start, Point *end, double startAngle, vector<Point> &interestingPoints, vector<Point*> &hullPoints);

	/**	Calculates an approximative linearized angle between two points, giving an angle between 0 - 8 instead of 0 - 2 * pi. 
	*	Will only use the old angle if otherwise the returned angle is smaller than the old angle, in which case 8 will be added
	*	to the angle to represent a new winding.
	*	
	*	Warning: Don't use to calculate a real angle, only to compare two angles. 
	*
	*	@param pointA the first Point
	*	@param pointB the second Point
	*	@param oldAngle the old angle to use as reference
	*	@return angle 
	*/
	double static getAngle(Point *pointA, Point *pointB, double oldAngle);
};

#endif